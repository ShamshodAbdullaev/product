package uz.pdp.model;

public class Tv extends Electronics{
    private double diaganal;
    private String format;
    private String color;
    private String tvNumber;

    public Tv() {
    }

    public Tv(double diaganal, String format, String color, String tvNumber) {
        this.diaganal = diaganal;
        this.format = format;
        this.color = color;
        this.tvNumber = tvNumber;
    }

    public double getDiaganal() {
        return diaganal;
    }

    public void setDiaganal(double diaganal) {
        this.diaganal = diaganal;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTvNumber() {
        return tvNumber;
    }

    public void setTvNumber(String tvNumber) {
        this.tvNumber = tvNumber;
    }

    @Override
    public String toString() {
        return "Tv{" +
                "diaganal=" + diaganal +
                ", format='" + format + '\'' +
                ", color='" + color + '\'' +
                ", tvNumber='" + tvNumber + '\'' +
                ", voltage=" + voltage +
                ", brand='" + brand + '\'' +
                ", warranty='" + warranty + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", madeIn='" + madeIn + '\'' +
                '}';
    }
}
