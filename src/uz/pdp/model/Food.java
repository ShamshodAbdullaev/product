package uz.pdp.model;

public abstract class Food extends Product{
    protected String  expireDate;
    protected double colorie;

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public double getColorie() {
        return colorie;
    }

    public void setColorie(double colorie) {
        this.colorie = colorie;
    }

}
