package uz.pdp.model;

public class Vagetable extends Food {
        private String color;
        private String type;
        private int timeWork;
        private String vagetableNumber;

    public Vagetable() {
    }

    public Vagetable(String color, String type, int timeWork, String vagetableNumber) {
        this.color = color;
        this.type = type;
        this.timeWork = timeWork;
        this.vagetableNumber = vagetableNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTimeWork() {
        return timeWork;
    }

    public void setTimeWork(int timeWork) {
        this.timeWork = timeWork;
    }

    public String getVagetableNumber() {
        return vagetableNumber;
    }

    public void setVagetableNumber(String vagetableNumber) {
        this.vagetableNumber = vagetableNumber;
    }

    @Override
    public String toString() {
        return "Vagetable{" +
                "color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", timeWork=" + timeWork +
                ", vagetableNumber='" + vagetableNumber + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", colorie=" + colorie +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", madeIn='" + madeIn + '\'' +
                '}';
    }
}
