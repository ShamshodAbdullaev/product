package uz.pdp.model;

public class Phone extends Electronics {
    private String model;
    private String os;
    private int memory;
    private  int ram;
    private double pixel;
    private String phoneNumber;

    public Phone() {
    }

    public Phone(String model, String os, int memory, int ram, double pixel, String phoneNumber) {
        this.model = model;
        this.os = os;
        this.memory = memory;
        this.ram = ram;
        this.pixel = pixel;
        this.phoneNumber = phoneNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public double getPixel() {
        return pixel;
    }

    public void setPixel(double pixel) {
        this.pixel = pixel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "model='" + model + '\'' +
                ", os='" + os + '\'' +
                ", memory=" + memory +
                ", ram=" + ram +
                ", pixel=" + pixel +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", voltage=" + voltage +
                ", brand='" + brand + '\'' +
                ", warranty='" + warranty + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", madeIn='" + madeIn + '\'' +
                '}';
    }
}
