package uz.pdp.model;

import uz.pdp.model.Fruit;
import uz.pdp.model.Phone;
import uz.pdp.model.Tv;
import uz.pdp.model.Vagetable;
import uz.pdp.service.ProductService;

import java.util.Arrays;
import java.util.Scanner;

public class Market extends ProductService {
    Scanner scanner = new Scanner(System.in);

    Product[] products = new Product[200];
    Phone[] phones = new Phone[50];
    Tv[] tvs = new Tv[50];
    Fruit[] fruits = new Fruit[50];
    Vagetable[] vagetables = new Vagetable[50];

    public void productMassiv(){
        System.arraycopy(phones,0,products,0,phones.length);
        System.arraycopy(tvs,0,products,phones.length,tvs.length);
        System.arraycopy(fruits,0,products,phones.length+tvs.length,fruits.length);
        System.arraycopy(vagetables,0,products,phones.length+tvs.length+fruits.length,fruits.length);

    }

    Phone phone = new Phone();
    Tv tv = new Tv();
    Fruit fruit = new Fruit();
    Vagetable vagetable = new Vagetable();

    @Override
    public void requestProduct() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Electronics, 2=> Foods \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    addElectronics();
                    break;
                case 2:
                    addFoods();
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }

    @Override
    public void editProduct() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Electronics, 2=> Foods \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    editElectronics();
                    break;
                case 2:
                    editFoods();
                    break;
                default:
                    System.out.println("");
            }
        }
    }

    @Override
    public void responseProduct() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Electronics, 2=> Foods \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    responseElectronics();
                    break;
                case 2:
                    responseFoods();
                    break;
                default:
                    System.out.println("");
            }
        }
    }

    @Override
    public void deleteProduct() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Phone, 2=> TV, 3=> Fruit, 4=> Vagetable \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("PhoneNumberni kiriting: ");
                    String phoneNumber = scanner.nextLine();
                    for (int i = 0; i < products.length; i++) {
                        if (phone.getPhoneNumber().equals(phoneNumber)) {
                            products[i] = null;
                        }
                    }
                    break;
                case 2:
                    scanner = new Scanner(System.in);
                    System.out.print("TvNumberni kiriting: ");
                    String tvNumber = scanner.nextLine();
                    for (int i = 0; i < products.length; i++) {
                        if (tv.getTvNumber().equals(tvNumber)) {
                            products[i] = null;
                        }
                    }
                    break;
                case 3:
                    scanner = new Scanner(System.in);
                    System.out.print("FruitNumberni kiriting: ");
                    String fruitNumber = scanner.nextLine();
                    for (int i = 0; i < products.length; i++) {
                        if (fruit.getFruitNumber().equals(fruitNumber)) {
                            products[i] = null;
                        }
                    }
                    break;
                case 4:
                    scanner = new Scanner(System.in);
                    System.out.print("VagetableNumberni kiriting: ");
                    String vagetableNumber = scanner.nextLine();
                    for (int i = 0; i < products.length; i++) {
                        if (vagetable.getVagetableNumber().equals(vagetableNumber)) {
                            products[i] = null;
                        }
                    }
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }


    private void addElectronics() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> PHONE, 2=> TV \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("Phone NAMEni kiriting: ");
                    phone.setName(scanner.nextLine());
                    System.out.print("Phone MADEINni kiriting: ");
                    phone.setMadeIn(scanner.nextLine());
                    System.out.print("Phone BRANDni kiriting: ");
                    phone.setBrand(scanner.nextLine());
                    System.out.print("Phone WARRANTYni kiriting: ");
                    phone.setWarranty(scanner.nextLine());
                    System.out.print("Phone OSni kiriting: ");
                    phone.setOs(scanner.nextLine());
                    System.out.print("Phone MODELni kiriting: ");
                    phone.setModel(scanner.nextLine());
                    System.out.print("Phone PhoneNumberni kiriting: ");
                    phone.setPhoneNumber(scanner.nextLine());

                    scanner = new Scanner(System.in);
                    System.out.print("Phone PRICEni kiriting: ");
                    phone.setPrice(scanner.nextDouble());
                    System.out.print("Phone VOLTAGEni kiriting: ");
                    phone.setVoltage(scanner.nextDouble());
                    System.out.print("Phone MEMORYni kiriting: ");
                    phone.setMemory(scanner.nextInt());
                    System.out.print("Phone RAMni kiriting: ");
                    phone.setRam(scanner.nextInt());
                    System.out.print("Phone PIXELni kiriting: ");
                    phone.setPixel(scanner.nextDouble());

                    for (int i = 0; i < products.length; i++) {
                        if (products[i] == null) {
                            products[i] = phone;
                            break;
                        }
                    }
                    break;
                case 2:
                    scanner = new Scanner(System.in);
                    System.out.print("TV NAMEni kiriting: ");
                    tv.setName(scanner.nextLine());
                    System.out.print("TV MADEINni kiriting: ");
                    tv.setMadeIn(scanner.nextLine());
                    System.out.print("TV BRANDni kiriting: ");
                    tv.setBrand(scanner.nextLine());
                    System.out.print("Tv WARRANTYni kiriting: ");
                    tv.setWarranty(scanner.nextLine());
                    System.out.print("TV FORMATni kiriting: ");
                    tv.setFormat(scanner.nextLine());
                    System.out.print("TV COLORni kiriting: ");
                    tv.setColor(scanner.nextLine());
                    System.out.print("TV TvNumberni kiriting: ");
                    tv.setTvNumber(scanner.nextLine());

                    scanner = new Scanner(System.in);
                    System.out.print("TV PRICEni kiriting: ");
                    tv.setPrice(scanner.nextDouble());
                    System.out.print("TV VOLTAGEni kiriting: ");
                    tv.setVoltage(scanner.nextDouble());
                    System.out.print("TV DIAGANALni kiriting: ");
                    tv.setDiaganal(scanner.nextInt());

                    for (int i = 0; i < products.length; i++) {
                        if (products[i] == null) {
                            products[i] = tv;
                            break;
                        }
                    }
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }

    private void addFoods() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Fruits, 2=> Vagetables \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("Fruit NAMEni kiriting: ");
                    fruit.setName(scanner.nextLine());
                    System.out.print("Fruit MADEINni kiriting: ");
                    fruit.setMadeIn(scanner.nextLine());
                    System.out.print("Fruit EXPIREDATEni kiriting: ");
                    fruit.setExpireDate(scanner.nextLine());
                    System.out.print("Fruit TYPEni kiriting: ");
                    fruit.setType(scanner.nextLine());
                    System.out.print("Fruit FruitNumberni kiriting: ");
                    fruit.setFruitNumber(scanner.nextLine());

                    scanner = new Scanner(System.in);
                    System.out.print("Fruit PRICEni kiriting: ");
                    fruit.setPrice(scanner.nextDouble());
                    System.out.print("Fruit COLORIEni kiriting: ");
                    fruit.setColorie(scanner.nextDouble());

                    for (int i = 0; i < products.length; i++) {
                        if (products[i] == null) {
                            products[i] = fruit;
                            break;
                        }
                    }
                    break;
                case 2:
                    scanner = new Scanner(System.in);
                    System.out.print("Vagetable NAMEni kiriting: ");
                    vagetable.setName(scanner.nextLine());
                    System.out.print("Vagetable MADEINni kiriting: ");
                    vagetable.setMadeIn(scanner.nextLine());
                    System.out.print("Vagetable EXPIREDATEni kiriting: ");
                    vagetable.setExpireDate(scanner.nextLine());
                    System.out.print("Vagetable TYPEni kiriting: ");
                    vagetable.setType(scanner.nextLine());
                    System.out.print("Vagetable COLORni kiriting: ");
                    vagetable.setColor(scanner.nextLine());
                    System.out.print("Vagetable VagetableNumberni kiriting: ");
                    vagetable.setVagetableNumber(scanner.nextLine());

                    scanner = new Scanner(System.in);
                    System.out.print("Vagetable PRICEni kiriting: ");
                    vagetable.setPrice(scanner.nextDouble());
                    System.out.print("Vagetable COLORIEni kiriting: ");
                    vagetable.setColorie(scanner.nextDouble());
                    System.out.print("Vagetable timeWorkni kiriting: ");
                    vagetable.setTimeWork(scanner.nextInt());

                    for (int i = 0; i < products.length; i++) {
                        if (products[i] == null) {
                            products[i] = vagetable;
                            break;
                        }
                    }
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }

    private void editElectronics() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> PHONE, 2=> TV \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    scanner = new Scanner(System.in);
                    System.out.print("Phone PhoneNumberni kiriting: ");
                    String phoneNumber = scanner.nextLine();
                    for (int i = 0; i < phones.length; i++) {
                        if (phones[i].getModel().equals(phoneNumber)){
                            System.out.print("Phone NAMEni kiriting: ");
                            phones[i].setName(scanner.nextLine());
                            System.out.print("Phone MADEINni kiriting: ");
                            phones[i].setMadeIn(scanner.nextLine());
                            System.out.print("Phone BRANDni kiriting: ");
                            phones[i].setBrand(scanner.nextLine());
                            System.out.print("Phone WARRANTYni kiriting: ");
                            phones[i].setWarranty(scanner.nextLine());
                            System.out.print("Phone OSni kiriting: ");
                            phones[i].setOs(scanner.nextLine());
                            System.out.print("Phone MODELni kiriting: ");
                            phones[i].setModel(scanner.nextLine());
                            System.out.print("Phone PhoneNumberni kiriting: ");
                            phones[i].setPhoneNumber(scanner.nextLine());

                            scanner = new Scanner(System.in);
                            System.out.print("Phone PRICEni kiriting: ");
                            products[i].setPrice(scanner.nextDouble());
                            System.out.print("Phone VOLTAGEni kiriting: ");
                            phones[i].setVoltage(scanner.nextDouble());
                            System.out.print("Phone MEMORYni kiriting: ");
                            phones[i].setMemory(scanner.nextInt());
                            System.out.print("Phone RAMni kiriting: ");
                            phones[i].setRam(scanner.nextInt());
                            System.out.print("Phone PIXELni kiriting: ");
                            phones[i].setPixel(scanner.nextDouble());
                            break;
                        }
                    }
                    break;
                case 2:
                    scanner = new Scanner(System.in);
                    System.out.print("TV TvNumberni kiriting: ");
                    String tvNumber = scanner.nextLine();
                    for (int i = 0; i < products.length; i++) {

                    }
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }

    private void editFoods() {
        scanner = new Scanner(System.in);
        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Back, 1=> Fruits, 2=> Vagetables \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {

            }
        }
    }

    private void responseElectronics() {

    }

    private void responseFoods() {

    }

    @Override
    public String toString() {
        return "Market{" +
                "products=" + Arrays.toString(products) +
                '}';
    }
}