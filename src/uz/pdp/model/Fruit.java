package uz.pdp.model;

public class Fruit extends Food {

    private String type;
    private String fruitNumber;

    public Fruit() {
    }

    public Fruit(String type, String fruitNumber) {
        this.type = type;
        this.fruitNumber = fruitNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFruitNumber() {
        return fruitNumber;
    }

    public void setFruitNumber(String fruitNumber) {
        this.fruitNumber = fruitNumber;
    }

    @Override
    public String toString() {
        return "Fruit{" +
                "type='" + type + '\'' +
                ", fruitNumber='" + fruitNumber + '\'' +
                ", expireDate='" + expireDate + '\'' +
                ", colorie=" + colorie +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", madeIn='" + madeIn + '\'' +
                '}';
    }
}
