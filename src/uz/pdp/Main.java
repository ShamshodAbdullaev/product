package uz.pdp;

import uz.pdp.model.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        Market market = new Market();
        market.productMassiv();

        int stepCode = 1;
        while (stepCode != 0) {
            System.out.println("\n 0=> Exit, 1=> request PRODUCT, 2=> edit PRODUCT," +
                    " 3=> response PRODUCT, 4=> delete PRODUCT, 5=> view PRODUCTS \n");
            System.out.print("Menugadi SONlarni kiriting: ");
            stepCode = scanner.nextInt();
            switch (stepCode) {
                case 0:
                    break;
                case 1:
                    market.requestProduct();
                    break;
                case 2:
                    market.editProduct();
                    break;
                case 3:
                    market.responseProduct();
                    break;
                case 4:
                    market.deleteProduct();
                    break;
                case 5:
                    System.out.println(market.toString());
                    break;
                default:
                    System.out.println("Menugi sonlardan chiqib ketiz. Menugi sonlardan kiriting!!!");
            }
        }
    }

}
