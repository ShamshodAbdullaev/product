package uz.pdp.service;

import uz.pdp.model.*;

import java.util.Arrays;

public abstract class ProductService {

    public abstract void requestProduct();

    public abstract void editProduct();

    public abstract void responseProduct();

    public abstract void deleteProduct();
}
